[![Build Status](https://travis-ci.com/RonnyDo/ColorPicker.svg?branch=master)](https://travis-ci.com/RonnyDo/ColorPicker)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Color Picker

Disclaimer - Forked version of original work ->  [RonnyDo / ColorPicker](https://github.com/RonnyDo/ColorPicker). I have only added details about how to install on Fedora.

One Color Picker to rule them all! No overhelming menus or settings. An easy tool with the features you need.

Features:
* Pick a color with the zoomable Magnifier
* Choose between multiple Color Formats
* Let the Color History remember your last colors

![ColorPicker Screenshot](https://raw.github.com/ronnydo/colorpicker/master/data/screenshot.png)


## Build from source
If you like to build ColorPicker yourself, take a look at the [`dev-build.sh`](dev-build.sh) file.

### Dependencies
You'll need the following dependencies to compile ColorPicker:
* granite
* granite-devel
* meson
* vala

### Example

Here is how to install the package on your system. I have tested this on Fedora 32 fresh install

```sh
# installing depedencies
sudo dnf install granite granite-devel meson vala -y

# cloning this repo OR original repo
git clone https://gitlab.com/devarshirawal0111/colorpicker.git
# OR
git clone https://github.com/RonnyDo/ColorPicker.git

# building from source
cd ColorPicker/
chmod +x dev-build.sh
./dev-build.sh

# creating menu entry
cd ~/.local/share/applications
cat > colorpicker.desktop <<- "EOF"
Name=Color Picker
Exec=<path to colorpicker>/build/com.github.ronnydo.colorpicker
Comment=Elementary Color Picker
Keywords=colorpicker;color;picker;selector;pipette;
Terminal=false
Icon=<path to colorpicker>/data/icons/128/com.github.ronnydo.colorpicker.svg
Type=Application
Categories=GTK;Utility;Graphics;Development;
EOF


```
